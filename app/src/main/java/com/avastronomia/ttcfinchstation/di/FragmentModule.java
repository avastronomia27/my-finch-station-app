package com.avastronomia.ttcfinchstation.di;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.avastronomia.ttcfinchstation.util.ActivityRouter;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    private Fragment fragment;

    public FragmentModule(Fragment fragment){
        this.fragment = fragment;
    }

    @PerFragment
    @Provides
    ActivityRouter providesActivityRouter(){
        FragmentActivity activity = fragment.getActivity();
        if(activity != null){
            return new ActivityRouter(activity);
        }else{
            return null;
        }
    }

}
