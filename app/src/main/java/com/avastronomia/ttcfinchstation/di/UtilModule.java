package com.avastronomia.ttcfinchstation.di;

import android.content.Context;

import com.avastronomia.ttcfinchstation.util.ToastUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilModule {

    @Provides
    @Singleton
    ToastUtil providesToastUtil(@AppContext Context context){
        return new ToastUtil(context);
    }
}
