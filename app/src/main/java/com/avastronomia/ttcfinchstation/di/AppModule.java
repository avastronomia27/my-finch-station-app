package com.avastronomia.ttcfinchstation.di;

import android.app.Application;
import android.content.Context;

import com.avastronomia.ttcfinchstation.model.AlertRepository;
import com.avastronomia.ttcfinchstation.model.TtcFinchRepository;
import com.avastronomia.ttcfinchstation.model.WeatherRepository;
import com.avastronomia.ttcfinchstation.usecase.AlertUseCase;
import com.avastronomia.ttcfinchstation.usecase.StopUseCase;
import com.avastronomia.ttcfinchstation.usecase.WeatherUseCase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes={NetworkModule.class, UtilModule.class})
public class AppModule {

    private Application app;

    public AppModule(Application app){this.app = app;}

    @Singleton
    @Provides
    Application provideApplicatioon(){return app;}

    @Provides
    @Singleton
    @AppContext
    Context providesContext(){ return app;}

    @Singleton
    @Provides
    public StopUseCase providesStopUseCase(TtcFinchRepository ttcFinchRepository){
        return new StopUseCase(ttcFinchRepository);
    }

    @Singleton
    @Provides
    public WeatherUseCase providesWeatherUseCase(WeatherRepository weatherRepository){
        return new WeatherUseCase(weatherRepository);
    }

    @Singleton
    @Provides
    public AlertUseCase providesAlertUseCase(AlertRepository alertRepository){
        return new AlertUseCase(alertRepository);
    }
}
