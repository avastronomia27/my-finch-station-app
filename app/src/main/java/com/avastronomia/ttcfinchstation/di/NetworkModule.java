package com.avastronomia.ttcfinchstation.di;

import com.avastronomia.ttcfinchstation.BuildConfig;
import com.avastronomia.ttcfinchstation.service.AlertDataService;
import com.avastronomia.ttcfinchstation.service.TtcDataService;
import com.avastronomia.ttcfinchstation.service.WeatherDataService;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.avastronomia.ttcfinchstation.util.Constants.ALERTS_URL;
import static com.avastronomia.ttcfinchstation.util.Constants.BASE_URL;
import static com.avastronomia.ttcfinchstation.util.Constants.TEMPERATURE_URL;

@Module
public class NetworkModule {

    public NetworkModule(){
    }

    @Provides
    @Singleton
    TtcDataService provideTtcDataService(@Named("Ttc")Retrofit retrofit){return retrofit.create(TtcDataService.class);}

    @Provides
    @Singleton
    WeatherDataService provideWeatherDataService(@Named("temperature")Retrofit retrofit){
            return retrofit.create(WeatherDataService.class);
    }

    @Provides
    @Singleton
    AlertDataService providesAlertDataService(@Named("Alerts")Retrofit retrofit){
        return retrofit.create(AlertDataService.class);
    }


    @Provides
    @Singleton
    @Named("Ttc")
    Retrofit providesRetrofit(OkHttpClient okHttpClient, RxJava2CallAdapterFactory rxJava2CallAdapterFactory, GsonConverterFactory gsonConverterFactory){
        return new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Singleton
    @Named("temperature")
    Retrofit providesWeatherRetrofit(OkHttpClient okHttpClient, RxJava2CallAdapterFactory rxJava2CallAdapterFactory, GsonConverterFactory gsonConverterFactory){
        return new Retrofit
                .Builder()
                .baseUrl(TEMPERATURE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Singleton
    @Named("Alerts")
    Retrofit providesAlertsRetrofit(OkHttpClient okHttpClient, RxJava2CallAdapterFactory rxJava2CallAdapterFactory, GsonConverterFactory gsonConverterFactory){
        return new Retrofit
                .Builder()
                .baseUrl(ALERTS_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Singleton
    @Provides
    OkHttpClient getOkHttpClient(){
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        return new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(logger)
                .build();
    }

    @Singleton
    @Provides
    RxJava2CallAdapterFactory profidesRxJava2CallAdapterFactory(){
        return RxJava2CallAdapterFactory.create();
    }

    @Singleton
    @Provides
    GsonConverterFactory providesGsonConverterFactory(){
        return GsonConverterFactory.create();
    }
}
