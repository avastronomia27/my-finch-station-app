package com.avastronomia.ttcfinchstation.di;

import android.app.Activity;

import com.avastronomia.ttcfinchstation.App;
import com.avastronomia.ttcfinchstation.view.MainActivity;
import com.avastronomia.ttcfinchstation.view.RouteActivity;
import com.avastronomia.ttcfinchstation.view.ShapeActivity;
import com.avastronomia.ttcfinchstation.view.SplashScreenActivity;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashScreenActivity splashScreenActivity);
    void inject(MainActivity mainActivity);
    void inject(RouteActivity routeActivity);
    void inject(ShapeActivity shapeActivity);

    public static ActivityComponent component(Activity activity){
        return App.component(activity).plusActivityModule(new ActivityModule(activity));
    }
}
