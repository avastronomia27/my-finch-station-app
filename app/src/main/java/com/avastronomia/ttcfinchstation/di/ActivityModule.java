package com.avastronomia.ttcfinchstation.di;

import android.app.Activity;
import android.content.Context;

import com.avastronomia.ttcfinchstation.util.ActivityRouter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext(){
        return activity;
    }

    @Provides
    Activity provideActivity(){return activity;}

    @Provides
    ActivityRouter providesActivityRouter(){
        return new ActivityRouter(activity);
    }
}
