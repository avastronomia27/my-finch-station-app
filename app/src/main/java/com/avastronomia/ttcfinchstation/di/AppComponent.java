package com.avastronomia.ttcfinchstation.di;

import android.app.Application;

import com.avastronomia.ttcfinchstation.App;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(App app);

    ActivityComponent plusActivityModule(ActivityModule activityModule);
    FragmentComponent plusFragmentModule(FragmentModule fragmentModule);

    public static AppComponent component(Application application){
        return DaggerAppComponent.builder().appModule(new AppModule(application)).networkModule(new NetworkModule()).build();
    }
}
