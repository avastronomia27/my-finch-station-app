package com.avastronomia.ttcfinchstation.di;

import androidx.fragment.app.Fragment;

import com.avastronomia.ttcfinchstation.App;
import com.avastronomia.ttcfinchstation.view.AlertFragment;
import com.avastronomia.ttcfinchstation.view.HomeFragment;
import com.avastronomia.ttcfinchstation.view.StationFragment;

import dagger.Subcomponent;

@PerFragment
@Subcomponent(modules = {FragmentModule.class})
public interface FragmentComponent {
    void inject(StationFragment stationFragment);
    void inject(HomeFragment homeFragment);
    void inject(AlertFragment alertFragment);

    public static FragmentComponent component(Fragment fragment){
        if(fragment.getActivity() != null){
            return App.component(fragment.getActivity()).plusFragmentModule(new FragmentModule(fragment));
        }
        else{
            return null;
        }
    }
}
