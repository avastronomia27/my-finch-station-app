package com.avastronomia.ttcfinchstation.util;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.view.AlarmDepartureBroadcast;
import com.avastronomia.ttcfinchstation.view.MainActivity;
import com.avastronomia.ttcfinchstation.view.RouteActivity;
import com.avastronomia.ttcfinchstation.view.ShapeActivity;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;
import javax.inject.Singleton;

import static android.content.Context.ALARM_SERVICE;
import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_ROUTES_NAME;
import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_ROUTES_URI;
import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_STOPS_NAME;
import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_STOPS_URI;
import static com.avastronomia.ttcfinchstation.util.Constants.SHAPE_NAME;

@Singleton
public class ActivityRouter {

    private Activity activity;

    @Inject
    public ActivityRouter(Activity activity){
        this.activity = activity;
    }

    public void startMainActivity(){
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    public void startRouteActivity(String stopUri,
                                   String stopName){
        Intent intent = new Intent(activity, RouteActivity.class);
        intent.putExtra(EXTRA_INTENT_STOPS_NAME, stopName);
        intent.putExtra(EXTRA_INTENT_STOPS_URI, stopUri);
        activity.startActivity(intent);
    }

    public void startShapeActivity(String stopUri,
                                   String routeUri,
                                   String routeName) {
        Intent intent = new Intent(activity, ShapeActivity.class);
        intent.putExtra(EXTRA_INTENT_STOPS_URI, stopUri);
        intent.putExtra(EXTRA_INTENT_ROUTES_URI, routeUri);
        intent.putExtra(EXTRA_INTENT_ROUTES_NAME, routeName);
        activity.startActivity(intent);
    }

    public void setAlarmDepartureDialog(String shapeName, long departureTimestamp){
        createNotificationChannel(activity);
        long alarmTimestamp = (departureTimestamp * 1000) - 300000;
        Intent intent = new Intent(activity, AlarmDepartureBroadcast.class);
        intent.putExtra(SHAPE_NAME, shapeName);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(activity, 0, intent, 0);

        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                alarmTimestamp,
                pendingIntent);
    }

    private void createNotificationChannel(Context context){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "TtcReminderChannel";
            String description = "Channel for TTC Finch Reminder";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("finchNotify", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void openNewsArticle(String articleLink){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(articleLink));
        activity.startActivity(intent);
    }

}
