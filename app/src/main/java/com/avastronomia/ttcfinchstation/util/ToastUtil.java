package com.avastronomia.ttcfinchstation.util;

import android.content.Context;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ToastUtil {
    private Context context;

    @Inject
    public ToastUtil(Context context){
        this.context = context;
    }

    public void showToastMessage(String message){
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
    }
}
