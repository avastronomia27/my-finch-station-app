package com.avastronomia.ttcfinchstation.util;

public class Constants {
    public static final String BASE_URL = "https://myttc.ca/";
    public static final String TEMPERATURE_URL = "http://api.openweathermap.org/";
    public static final String ALERTS_URL = "http://eventregistry.org/";

    public static final String EXTRA_INTENT_STOPS_NAME ="com.avastronomia.ttcfinchstation.view.StationFragment.EXTRA_INTENT_STOPS.NAME";
    public static final String EXTRA_INTENT_STOPS_URI ="com.avastronomia.ttcfinchstation.view.StationFragment.EXTRA_INTENT_STOPS.URI";

    public static final String EXTRA_INTENT_ROUTES_NAME ="com.avastronomia.ttcfinchstation.view.RouteActivity.EXTRA_INTENT_ROUTES.NAME";
    public static final String EXTRA_INTENT_ROUTES_URI ="com.avastronomia.ttcfinchstation.view.RouteActivity.EXTRA_INTENT_ROUTES.URI";

    public static final String SHAPE_NAME = "com.avastronomia.ttcfinchstation.R.SHAPE_NAME";

    public static final String WEATHER_LOCATION = "toronto,ca";
    public static final String WEATHER_UNIT = "metric";
    public static final String WEATHER_APIKEY = "0fcf0970c5aee9aaa5f6e780bfd96ddc";

    public static final String DUMMY_FOR_ALERTS_ACTION = "getArticles";
    public static final String DUMMY_FOR_ALERTS_KEYWORD = "Transportation Toronto";
    public static final String DUMMY_FOR_ALERTS_SORTBY = "date";
    public static final String DUMMY_FOR_ALERTS_RESULTTYPE = "articles";
    public static final String DUMMY_FOR_ALERTS_API_KEY = "a5ae3648-e9ac-4074-a479-824b42306ec8";
}
