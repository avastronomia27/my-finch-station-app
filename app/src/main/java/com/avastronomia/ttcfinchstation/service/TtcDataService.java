package com.avastronomia.ttcfinchstation.service;

import com.avastronomia.ttcfinchstation.model.ttc.TtcFinchResponse;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface TtcDataService {

    @GET("finch_station.json")
    Observable<TtcFinchResponse> getTtcFinchList();
}
