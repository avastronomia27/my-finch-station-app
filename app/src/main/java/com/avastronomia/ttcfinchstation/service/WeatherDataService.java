package com.avastronomia.ttcfinchstation.service;

import com.avastronomia.ttcfinchstation.model.weather.WeatherResponse;


import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherDataService {

    @GET("data/2.5/weather")
    Single<WeatherResponse> getCurrentWeather(@Query("q") String locationName,
                                              @Query("appid") String apiKey,
                                              @Query("units") String units);

}
