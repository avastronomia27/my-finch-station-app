package com.avastronomia.ttcfinchstation.service;

import com.avastronomia.ttcfinchstation.model.alert.AlertRequest;
import com.avastronomia.ttcfinchstation.model.alert.AlertResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AlertDataService {
    @POST("api/v1/article/getArticles")
    Observable<AlertResponse> getArticleList(@Body AlertRequest alertRequest);
}
