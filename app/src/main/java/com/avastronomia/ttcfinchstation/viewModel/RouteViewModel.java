package com.avastronomia.ttcfinchstation.viewModel;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.avastronomia.ttcfinchstation.model.ttc.Route;
import com.avastronomia.ttcfinchstation.usecase.StopUseCase;
import com.avastronomia.ttcfinchstation.util.ToastUtil;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RouteViewModel extends ViewModel {

    private final StopUseCase stopUseCase;
    private final ToastUtil toastUtil;

    private MutableLiveData<List<Route>> routeLiveData  = new MutableLiveData<>();
    public ObservableField<Boolean> isDisplayLoading;

    @Inject
    public RouteViewModel(StopUseCase stopUseCase,
                          ToastUtil toastUtil){
        this.stopUseCase = stopUseCase;
        this.toastUtil = toastUtil;
        isDisplayLoading = new ObservableField<>(true);
    }

    public void loadAllRoute(String stopUri){
        stopUseCase.getStopRouteList(stopUri)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Route>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Route> routes) {
                        routeLiveData.postValue(routes);
                        isDisplayLoading.set(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        isDisplayLoading.set(false);
                        toastUtil.showToastMessage("Unable to connect to the server.");
                    }
                });
    }

    public LiveData<List<Route>> getRouteLiveData(){
        return routeLiveData;
    }
}
