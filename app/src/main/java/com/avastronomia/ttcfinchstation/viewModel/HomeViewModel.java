package com.avastronomia.ttcfinchstation.viewModel;

import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;
import com.avastronomia.ttcfinchstation.usecase.WeatherUseCase;
import com.avastronomia.ttcfinchstation.util.ToastUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends ViewModel {

    private final WeatherUseCase weatherUseCase;
    private final ToastUtil toastUtil;

    public ObservableField<String> currentTemperature = new ObservableField<>();
    public ObservableField<String> currentDate;

    @Inject
    public HomeViewModel(WeatherUseCase weatherUseCase,
                         ToastUtil toastUtil){
        this.weatherUseCase = weatherUseCase;
        this.toastUtil = toastUtil;
        initDate();
        initTemperature();
    }

    private void initDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.CANADA);
        currentDate = new ObservableField<>(dateFormat.format(Calendar.getInstance().getTime()));
    }

    private void initTemperature(){
        weatherUseCase.getTemperature()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Double>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(Double aDouble) {
                        currentTemperature.set(aDouble.toString() + "\u00B0");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        toastUtil.showToastMessage("Unable to connect to the server.");
                    }
                });
    }
}
