package com.avastronomia.ttcfinchstation.viewModel;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.avastronomia.ttcfinchstation.model.ttc.Shape;
import com.avastronomia.ttcfinchstation.usecase.StopUseCase;
import com.avastronomia.ttcfinchstation.util.ToastUtil;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ShapeViewModel extends ViewModel {

    private final StopUseCase stopUseCase;
    private final ToastUtil toastUtil;

    private MutableLiveData<List<Shape>> shapesLiveData = new MutableLiveData<>();
    public ObservableField<Boolean> isDisplayLoading;

    @Inject
    public ShapeViewModel(StopUseCase stopUseCase,
                          ToastUtil toastUtil){
        this.stopUseCase = stopUseCase;
        this.toastUtil = toastUtil;
        isDisplayLoading = new ObservableField<>(true);
    }

    public void loadAllShape(String stopUri, String routeUri){
        stopUseCase.getStopShapeList(stopUri, routeUri)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Shape>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onSuccess(List<Shape> shapes) {
                        shapesLiveData.postValue(shapes);
                        isDisplayLoading.set(false);
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        isDisplayLoading.set(false);
                        toastUtil.showToastMessage("Unable to connect to the server.");
                    }
                });
    }

    public LiveData<List<Shape>> getShapesLiveData(){
        return shapesLiveData;
    }
}
