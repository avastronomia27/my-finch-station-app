package com.avastronomia.ttcfinchstation.viewModel;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.avastronomia.ttcfinchstation.model.ttc.Stop;
import com.avastronomia.ttcfinchstation.usecase.StopUseCase;
import com.avastronomia.ttcfinchstation.util.ToastUtil;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class StationViewModel extends ViewModel {

    private final StopUseCase stopUseCase;
    private final ToastUtil toastUtil;

    private MutableLiveData<List<Stop>> stopsLiveData;
    private CompositeDisposable compositeDisposable;
    public ObservableField<Boolean> isDisplayLoading;


    @Inject
    public StationViewModel(StopUseCase stopUseCase,
                            ToastUtil toastUtil){
        this.stopUseCase = stopUseCase;
        this.toastUtil = toastUtil;
        stopsLiveData = new MutableLiveData<>();
        compositeDisposable = new CompositeDisposable();
        isDisplayLoading = new ObservableField<>(true);
        loadAllStops();
    }

    public void loadAllStops(){
        stopUseCase.getStopList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Stop>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }
                    @Override
                    public void onSuccess(List<Stop> stops) {
                        stopsLiveData.postValue(stops);
                        isDisplayLoading.set(false);
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        isDisplayLoading.set(false);
                        toastUtil.showToastMessage("Unable to connect to the server.");
                    }
                });
    }

    public LiveData<List<Stop>> getStopLiveData(){
        return stopsLiveData;
    }

    public void clear(){
        if(compositeDisposable != null){
            compositeDisposable.clear();
        }
    }
}
