package com.avastronomia.ttcfinchstation.viewModel;

import android.widget.Toast;

import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.avastronomia.ttcfinchstation.model.alert.AlertRequest;
import com.avastronomia.ttcfinchstation.model.alert.Result;
import com.avastronomia.ttcfinchstation.usecase.AlertUseCase;
import com.avastronomia.ttcfinchstation.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.avastronomia.ttcfinchstation.util.Constants.DUMMY_FOR_ALERTS_ACTION;
import static com.avastronomia.ttcfinchstation.util.Constants.DUMMY_FOR_ALERTS_API_KEY;
import static com.avastronomia.ttcfinchstation.util.Constants.DUMMY_FOR_ALERTS_KEYWORD;
import static com.avastronomia.ttcfinchstation.util.Constants.DUMMY_FOR_ALERTS_RESULTTYPE;
import static com.avastronomia.ttcfinchstation.util.Constants.DUMMY_FOR_ALERTS_SORTBY;

public class AlertViewModel {

    private final AlertUseCase alertUseCase;
    private final ToastUtil toastUtil;

    private MutableLiveData<List<Result>> resultListLiveData;
    public ObservableField<Boolean> isDisplayLoading;

    @Inject
    public AlertViewModel(AlertUseCase alertUseCase,
                          ToastUtil toastUtil){
        this.alertUseCase = alertUseCase;
        this.toastUtil = toastUtil;
        resultListLiveData = new MutableLiveData<>();
        isDisplayLoading = new ObservableField<>(true);
        loadAllAlerts();
    }

    private AlertRequest createAlertRequest(){
        List<String> newsType = new ArrayList<>();
        newsType.add("news");
        newsType.add("pr");
        return new AlertRequest(DUMMY_FOR_ALERTS_ACTION,
                DUMMY_FOR_ALERTS_KEYWORD,
                1,
                10,
                DUMMY_FOR_ALERTS_SORTBY,
                false,
                -1,
                DUMMY_FOR_ALERTS_RESULTTYPE,
                newsType,
                DUMMY_FOR_ALERTS_API_KEY,
                50);
    }

    public void loadAllAlerts(){
        alertUseCase.getArticleList(createAlertRequest())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Result>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Result> results) {
                        resultListLiveData.setValue(results);
                        isDisplayLoading.set(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        isDisplayLoading.set(false);
                        toastUtil.showToastMessage("Unable to connect to the server.");
                    }
                });
    }

    public LiveData<List<Result>> getResultsLiveData(){
        return resultListLiveData;
    }

}
