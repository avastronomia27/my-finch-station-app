package com.avastronomia.ttcfinchstation.usecase;

import com.avastronomia.ttcfinchstation.model.AlertRepository;
import com.avastronomia.ttcfinchstation.model.alert.AlertRequest;
import com.avastronomia.ttcfinchstation.model.alert.AlertResponse;
import com.avastronomia.ttcfinchstation.model.alert.Articles;
import com.avastronomia.ttcfinchstation.model.alert.Result;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AlertUseCase {
    private final AlertRepository alertRepository;

    @Inject
    public AlertUseCase(AlertRepository alertRepository){
        this.alertRepository = alertRepository;
    }

    public Single<List<Result>> getArticleList(AlertRequest alertRequest){
        return Single.fromObservable(alertRepository.getAlertList(alertRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(AlertResponse::getArticles)
                .map(Articles::getResults)
                .filter(result -> result.size() > 0));
    }

}
