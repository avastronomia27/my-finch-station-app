package com.avastronomia.ttcfinchstation.usecase;

import android.text.format.DateFormat;

import com.avastronomia.ttcfinchstation.model.ttc.Route;
import com.avastronomia.ttcfinchstation.model.ttc.Shape;
import com.avastronomia.ttcfinchstation.model.ttc.Stop;
import com.avastronomia.ttcfinchstation.model.ttc.StopTime;
import com.avastronomia.ttcfinchstation.model.TtcFinchRepository;
import com.avastronomia.ttcfinchstation.model.ttc.TtcFinchResponse;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class StopUseCase {
    private final TtcFinchRepository ttcFinchRepository;

    @Inject
    public StopUseCase(TtcFinchRepository ttcFinchRepository){
        this.ttcFinchRepository = ttcFinchRepository;
    }

    public Single<List<Stop>> getStopList(){
        return ttcFinchRepository.getAvailableStop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(TtcFinchResponse::getStops)
                .flatMapIterable(response -> response)
                .filter(stop -> stop.getRoutes().size() > 0)
                .toList();
    }

    public Single<List<Route>> getStopRouteList(String stopUri){
        return ttcFinchRepository.getAvailableStop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(TtcFinchResponse::getStops)
                .flatMapIterable(response -> response)
                .filter(stop->stop.getUri().equalsIgnoreCase(stopUri))
                .map(Stop::getRoutes)
                .flatMapIterable(stop->stop)
                .toList();
    }

    public Single<List<Shape>> getStopShapeList(String stopUri, String routeUri){
        return ttcFinchRepository.getAvailableStop()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(TtcFinchResponse::getStops)
                .flatMapIterable(respone -> respone)
                .filter(stop -> stop.getUri().equalsIgnoreCase(stopUri))
                .map(Stop::getRoutes)
                .flatMapIterable(stop -> stop)
                .filter(route -> route.getUri().equalsIgnoreCase(routeUri))
                .map(Route::getStopTimes)
                .flatMapIterable(route -> {
                    for(StopTime stopTime: route){
                        stopTime.setDepartureTime(formateTime(stopTime.getDepartureTimestamp()));
                    }
                    return route;
                })
                .groupBy(StopTime::getShape)
                .flatMapSingle(Observable::toList)
                .map(stopTimes -> {
                    return new Shape(stopTimes.get(0).getShape(), stopTimes);
                })
                .toList();
    }

    private String formateTime(long timeStamp){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timeStamp * 1000);
        String newTime = DateFormat.format("hh:mm a", cal).toString();
        return newTime;
    }

}
