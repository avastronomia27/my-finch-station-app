package com.avastronomia.ttcfinchstation.usecase;

import com.avastronomia.ttcfinchstation.model.WeatherRepository;

import javax.inject.Inject;

import io.reactivex.Single;

public class WeatherUseCase {
    private final WeatherRepository weatherRepository;

    @Inject
    public WeatherUseCase(WeatherRepository weatherRepository){
        this.weatherRepository = weatherRepository;
    }

    public Single<Double> getTemperature(){
        return weatherRepository.getTemperature();
    }
}
