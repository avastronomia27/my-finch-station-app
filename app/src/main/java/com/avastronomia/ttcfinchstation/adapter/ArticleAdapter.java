package com.avastronomia.ttcfinchstation.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.databinding.AlertListItemBinding;
import com.avastronomia.ttcfinchstation.model.alert.Result;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;
import com.avastronomia.ttcfinchstation.view.AlertFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>{

    private ActivityRouter router;
    private List<Result> resultList;
    AlertFragment alertFragment;

    public ArticleAdapter(ActivityRouter router, List<Result> resultList, AlertFragment alertFragment) {
        this.router = router;
        this.resultList = resultList;
        this.alertFragment = alertFragment;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AlertListItemBinding binding = AlertListItemBinding.inflate(layoutInflater, parent, false);
        return new ArticleViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        Result currentResult = resultList.get(position);
        holder.bind(currentResult);

        Glide.with(alertFragment)
                .load(currentResult.getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.pbLoadImage.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.pbLoadImage.setVisibility(View.GONE);
                        return false;
                    }
                })
                .apply(new RequestOptions().placeholder(R.drawable.ic_place_holder).error(R.drawable.ic_place_holder))
                .into(holder.ivArticleThumb);
    }

    @Override
    public int getItemCount() {
        return resultList != null ? resultList.size() : 0;
    }

    public void setResultList(List<Result> resultList) {
        this.resultList = resultList;
        notifyDataSetChanged();
    }

    class ArticleViewHolder extends RecyclerView.ViewHolder{
        private AlertListItemBinding binding;
        private ProgressBar pbLoadImage;
        private ImageView ivArticleThumb;

        public ArticleViewHolder(@NonNull AlertListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            pbLoadImage = binding.pbLoadImage;
            ivArticleThumb = binding.ivArticleThumb;
        }

        public void bind(Result result){
            binding.setResultModel(result);
            binding.executePendingBindings();
            binding.cvArticle.setOnClickListener(view -> {
                router.openNewsArticle(result.getUrl());
            });
        }
    }
}
