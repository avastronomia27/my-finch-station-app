package com.avastronomia.ttcfinchstation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.avastronomia.ttcfinchstation.databinding.StopListItemBinding;
import com.avastronomia.ttcfinchstation.model.ttc.Stop;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;

import java.util.ArrayList;
import java.util.List;

public class StopAdapter extends RecyclerView.Adapter<StopAdapter.StopViewHolder> implements Filterable {

    private ActivityRouter router;
    private List<Stop> stopsList;
    private List<Stop> stopListFull;

    public StopAdapter(ActivityRouter router, List<Stop> stopsList) {
        this.router = router;
        this.stopsList = stopsList;
        stopListFull = new ArrayList<>(stopsList);
    }

    @NonNull
    @Override
    public StopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        StopListItemBinding binding = StopListItemBinding.inflate(layoutInflater, parent, false);
        return new StopViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull StopViewHolder holder, int position) {
        Stop currentStop = stopsList.get(position);
        holder.bind(currentStop);
    }

    @Override
    public int getItemCount() {
        return stopsList != null ? stopsList.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return stopFilter;
    }

    private Filter stopFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Stop> filteredList = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(stopListFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(Stop stop: stopListFull){
                    if(stop.getName().toLowerCase().contains(filterPattern)){
                        filteredList.add(stop);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            stopsList.clear();
            stopsList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };

    public void setStopsList(List<Stop> stopsList) {
        this.stopsList = stopsList;
        stopListFull = new ArrayList<>(stopsList);
        notifyDataSetChanged();
    }

    class StopViewHolder extends RecyclerView.ViewHolder{
        StopListItemBinding binding;

        public StopViewHolder(@NonNull StopListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Stop stop){
            binding.setStop(stop);
            if(stop.getUri().equalsIgnoreCase("finch_station_subway_platform")){
                binding.setIsTrain(true);
            }
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(View ->
                router.startRouteActivity(stop.getUri(), stop.getName()));
        }
    }
}
