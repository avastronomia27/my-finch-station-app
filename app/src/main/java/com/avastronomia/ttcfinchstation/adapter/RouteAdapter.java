package com.avastronomia.ttcfinchstation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.avastronomia.ttcfinchstation.databinding.RouteListItemBinding;
import com.avastronomia.ttcfinchstation.model.ttc.Route;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;

import java.util.ArrayList;
import java.util.List;

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.RouteViewHolder> implements Filterable {

    private ActivityRouter router;
    private List<Route> routeList;
    private List<Route> routeListFull;
    private String stopUri;

    public RouteAdapter(ActivityRouter router,
                        List<Route> routeList,
                        String stopUri) {
        this.router = router;
        this.routeList = routeList;
        this.stopUri = stopUri;
        routeListFull = new ArrayList<>(routeList);
    }

    @NonNull
    @Override
    public RouteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RouteListItemBinding binding = RouteListItemBinding.inflate(layoutInflater, parent, false);
        return new RouteViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RouteViewHolder holder, int position) {
        Route currentRoute = routeList.get(position);
        holder.bind(currentRoute);
    }

    @Override
    public int getItemCount() {
        return routeList != null ? routeList.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return routeFilter;
    }

    private Filter routeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Route> filteredList = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(routeListFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(Route route: routeListFull){
                    if(route.getName().toLowerCase().contains(filterPattern)){
                        filteredList.add(route);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            routeList.clear();
            routeList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };

    public void setRouteList(List<Route> routeList) {
        this.routeList = routeList;
        routeListFull = new ArrayList<>(routeList);
        notifyDataSetChanged();
    }

    class RouteViewHolder extends RecyclerView.ViewHolder{
        RouteListItemBinding binding;
        public RouteViewHolder(@NonNull RouteListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Route route){
            binding.setRoute(route);
            if(route.getUri().equalsIgnoreCase("yonge-university-spadina_subway")){
                binding.setIsTrain(true);
            }
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(view ->
                    router.startShapeActivity(stopUri, route.getUri(), route.getName()));
        }
    }
}
