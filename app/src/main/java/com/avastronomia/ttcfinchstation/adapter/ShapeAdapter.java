package com.avastronomia.ttcfinchstation.adapter;

import android.app.Activity;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.avastronomia.ttcfinchstation.databinding.ExpandableShapeListItemBinding;
import com.avastronomia.ttcfinchstation.model.ttc.Shape;
import com.avastronomia.ttcfinchstation.model.ttc.StopTime;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class ShapeAdapter extends RecyclerView.Adapter<ShapeAdapter.ShapeViewHolder> implements Filterable {
    private Activity activity;
    private List<Shape> shapeList;
    private List<Shape> shapeListFull;
    private StopTimeAdapter.OnItemClickListener onItemClickListener;

    public ShapeAdapter(Activity activity,
                        List<Shape> shapeList,
                        StopTimeAdapter.OnItemClickListener onItemClickListener){
        this.activity = activity;
        this.onItemClickListener = onItemClickListener;
        this.shapeList = shapeList;
        shapeListFull = new ArrayList<>(shapeList);
    }

    @NonNull
    @Override
    public ShapeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ExpandableShapeListItemBinding binding = ExpandableShapeListItemBinding.inflate(layoutInflater, parent, false);
        return new ShapeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ShapeViewHolder holder, int position) {
        Shape currentShape = shapeList.get(position);

        List<StopTime> currentStopTime = currentShape.getStopTimes();
        StopTimeAdapter stopTimeAdapter = new StopTimeAdapter(currentStopTime, onItemClickListener);
        RecyclerView recyclerView = holder.binding.rvStopTime;
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(stopTimeAdapter);
        holder.bind(currentShape);

    }

    @Override
    public int getItemCount() {
        return shapeList != null ? shapeList.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return shapeFilter;
    }

    private Filter shapeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Shape> filteredList = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(shapeListFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(Shape shape: shapeListFull){
                    if(shape.getShapeName().toLowerCase().contains(filterPattern)){
                        filteredList.add(shape);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            shapeList.clear();
            shapeList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };

    public void setShapeList(List<Shape> shapeList){
        this.shapeList = shapeList;
        shapeListFull = new ArrayList<>(shapeList);
        notifyDataSetChanged();
    }

    class ShapeViewHolder extends RecyclerView.ViewHolder{
        ExpandableShapeListItemBinding binding;
        private ConstraintLayout constraintLayout;
        private MaterialCardView cardView;

        public ShapeViewHolder(@NonNull ExpandableShapeListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            cardView = binding.cvExpandableShape;
            constraintLayout = binding.clExpandableView;
        }

        public void bind(Shape shape){
            binding.setShape(shape);
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(view -> {
                if(constraintLayout.getVisibility() == View.GONE){
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    constraintLayout.setVisibility(View.VISIBLE);
                    binding.setIsExpanded(true);
                }else{
                    constraintLayout.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    binding.setIsExpanded(false);
                }

            });
        }
    }
}
