package com.avastronomia.ttcfinchstation.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.avastronomia.ttcfinchstation.databinding.StopTimeListItemBinding;
import com.avastronomia.ttcfinchstation.model.ttc.StopTime;

import java.util.List;

public class StopTimeAdapter extends RecyclerView.Adapter<StopTimeAdapter.StopTimeViewHolder>{

    private OnItemClickListener onItemClickListener;
    private List<StopTime> stopTimeList;

    public StopTimeAdapter(List<StopTime> stopTimeList, OnItemClickListener onItemClickListener) {
        this.stopTimeList = stopTimeList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public StopTimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        StopTimeListItemBinding binding = StopTimeListItemBinding.inflate(layoutInflater, parent, false);
        return new StopTimeViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull StopTimeViewHolder holder, int position) {
        StopTime currentStopTime = stopTimeList.get(position);
        holder.bind(currentStopTime, onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return stopTimeList != null ? stopTimeList.size() : 0;
    }

    class StopTimeViewHolder extends RecyclerView.ViewHolder{
        private StopTimeListItemBinding binding;
        private StopTimeViewHolder(@NonNull StopTimeListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private void bind(StopTime stopTime, OnItemClickListener onItemClickListener){
            binding.setStopTime(stopTime);
            binding.executePendingBindings();
            binding.getRoot().setOnClickListener(view -> {
                if(onItemClickListener != null){
                    onItemClickListener.onItemClicked(binding.getRoot(),
                            stopTime.getDepartureTime(),
                            stopTime.getShape(),
                            stopTime.getDepartureTimestamp());
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClicked(View vh,
                           String departureTime,
                           String shapeName,
                           long departureTimestamp);
    }
}
