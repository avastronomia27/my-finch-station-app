
package com.avastronomia.ttcfinchstation.model.alert;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Source implements Parcelable
{

    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("dataType")
    @Expose
    private String dataType;
    @SerializedName("title")
    @Expose
    private String title;
    public final static Parcelable.Creator<Source> CREATOR = new Creator<Source>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Source createFromParcel(Parcel in) {
            return new Source(in);
        }

        public Source[] newArray(int size) {
            return (new Source[size]);
        }

    }
    ;

    protected Source(Parcel in) {
        this.uri = ((String) in.readValue((String.class.getClassLoader())));
        this.dataType = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Source() {
    }

    /**
     * 
     * @param dataType
     * @param title
     * @param uri
     */
    public Source(String uri, String dataType, String title) {
        super();
        this.uri = uri;
        this.dataType = dataType;
        this.title = title;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(uri);
        dest.writeValue(dataType);
        dest.writeValue(title);
    }

    public int describeContents() {
        return  0;
    }

}
