package com.avastronomia.ttcfinchstation.model.ttc;

import java.util.List;

public class Shape {
    private String shapeName;
    private List<StopTime> stopTimes;

    public Shape(String shapeName, List<StopTime> stopTimes) {
        this.shapeName = shapeName;
        this.stopTimes = stopTimes;
    }

    public String getShapeName() {
        return shapeName;
    }

    public void setShapeName(String shapeName) {
        this.shapeName = shapeName;
    }

    public List<StopTime> getStopTimes() {
        return stopTimes;
    }

    public void setStopTimes(List<StopTime> stopTimes) {
        this.stopTimes = stopTimes;
    }

    public boolean isExpandable(){
        return stopTimes.size() > 1;
    }
}
