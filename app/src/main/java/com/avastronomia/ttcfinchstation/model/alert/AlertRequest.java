package com.avastronomia.ttcfinchstation.model.alert;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlertRequest {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("keyword")
    @Expose
    private String keyword;
    @SerializedName("articlesPage")
    @Expose
    private Integer articlesPage;
    @SerializedName("articlesCount")
    @Expose
    private Integer articlesCount;
    @SerializedName("articlesSortBy")
    @Expose
    private String articlesSortBy;
    @SerializedName("articlesSortByAsc")
    @Expose
    private Boolean articlesSortByAsc;
    @SerializedName("articlesArticleBodyLen")
    @Expose
    private Integer articlesArticleBodyLen;
    @SerializedName("resultType")
    @Expose
    private String resultType;
    @SerializedName("dataType")
    @Expose
    private List<String> dataType = null;
    @SerializedName("apiKey")
    @Expose
    private String apiKey;
    @SerializedName("forceMaxDataTimeWindow")
    @Expose
    private Integer forceMaxDataTimeWindow;

    public AlertRequest() {
    }

    public AlertRequest(String action,
                        String keyword,
                        Integer articlesPage,
                        Integer articlesCount,
                        String articlesSortBy,
                        Boolean articlesSortByAsc,
                        Integer articlesArticleBodyLen,
                        String resultType,
                        List<String> dataType,
                        String apiKey,
                        Integer forceMaxDataTimeWindow) {
        this.action = action;
        this.keyword = keyword;
        this.articlesPage = articlesPage;
        this.articlesCount = articlesCount;
        this.articlesSortBy = articlesSortBy;
        this.articlesSortByAsc = articlesSortByAsc;
        this.articlesArticleBodyLen = articlesArticleBodyLen;
        this.resultType = resultType;
        this.dataType = dataType;
        this.apiKey = apiKey;
        this.forceMaxDataTimeWindow = forceMaxDataTimeWindow;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getArticlesPage() {
        return articlesPage;
    }

    public void setArticlesPage(Integer articlesPage) {
        this.articlesPage = articlesPage;
    }

    public Integer getArticlesCount() {
        return articlesCount;
    }

    public void setArticlesCount(Integer articlesCount) {
        this.articlesCount = articlesCount;
    }

    public String getArticlesSortBy() {
        return articlesSortBy;
    }

    public void setArticlesSortBy(String articlesSortBy) {
        this.articlesSortBy = articlesSortBy;
    }

    public Boolean getArticlesSortByAsc() {
        return articlesSortByAsc;
    }

    public void setArticlesSortByAsc(Boolean articlesSortByAsc) {
        this.articlesSortByAsc = articlesSortByAsc;
    }

    public Integer getArticlesArticleBodyLen() {
        return articlesArticleBodyLen;
    }

    public void setArticlesArticleBodyLen(Integer articlesArticleBodyLen) {
        this.articlesArticleBodyLen = articlesArticleBodyLen;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public List<String> getDataType() {
        return dataType;
    }

    public void setDataType(List<String> dataType) {
        this.dataType = dataType;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Integer getForceMaxDataTimeWindow() {
        return forceMaxDataTimeWindow;
    }

    public void setForceMaxDataTimeWindow(Integer forceMaxDataTimeWindow) {
        this.forceMaxDataTimeWindow = forceMaxDataTimeWindow;
    }

}
