package com.avastronomia.ttcfinchstation.model;

import com.avastronomia.ttcfinchstation.model.alert.AlertRequest;
import com.avastronomia.ttcfinchstation.model.alert.AlertResponse;
import com.avastronomia.ttcfinchstation.service.AlertDataService;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AlertRepository {
    private final AlertDataService alertDataService;

    @Inject
    public AlertRepository(AlertDataService alertDataService){
        this.alertDataService = alertDataService;
    }

    public Observable<AlertResponse> getAlertList(AlertRequest alertRequest){
        return alertDataService.getArticleList(alertRequest);
    }
}
