
package com.avastronomia.ttcfinchstation.model.ttc;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StopTime {

    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("departure_timestamp")
    @Expose
    private Long departureTimestamp;
    @SerializedName("service_id")
    @Expose
    private Long serviceId;
    @SerializedName("shape")
    @Expose
    private String shape;

    public StopTime(String departureTime, Long departureTimestamp, Long serviceId, String shape) {
        this.departureTime = departureTime;
        this.departureTimestamp = departureTimestamp;
        this.serviceId = serviceId;
        this.shape = shape;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Long getDepartureTimestamp() {
        return departureTimestamp;
    }

    public void setDepartureTimestamp(Long departureTimestamp) {
        this.departureTimestamp = departureTimestamp;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

}
