
package com.avastronomia.ttcfinchstation.model.alert;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AlertResponse implements Parcelable
{

    @SerializedName("articles")
    @Expose
    private Articles articles;
    public final static Parcelable.Creator<AlertResponse> CREATOR = new Creator<AlertResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public AlertResponse createFromParcel(Parcel in) {
            return new AlertResponse(in);
        }

        public AlertResponse[] newArray(int size) {
            return (new AlertResponse[size]);
        }

    }
    ;

    protected AlertResponse(Parcel in) {
        this.articles = ((Articles) in.readValue((Articles.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public AlertResponse() {
    }

    /**
     * 
     * @param articles
     */
    public AlertResponse(Articles articles) {
        super();
        this.articles = articles;
    }

    public Articles getArticles() {
        return articles;
    }

    public void setArticles(Articles articles) {
        this.articles = articles;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(articles);
    }

    public int describeContents() {
        return  0;
    }

}
