package com.avastronomia.ttcfinchstation.model;

import com.avastronomia.ttcfinchstation.model.ttc.TtcFinchResponse;
import com.avastronomia.ttcfinchstation.service.TtcDataService;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class TtcFinchRepository {
    private final TtcDataService ttcDataService;

    @Inject
    public TtcFinchRepository(TtcDataService  ttcDataService){
        this.ttcDataService = ttcDataService;
    }

    public Observable<TtcFinchResponse> getAvailableStop(){
        return ttcDataService.getTtcFinchList();
    }

}
