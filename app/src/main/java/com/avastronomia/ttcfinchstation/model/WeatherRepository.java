package com.avastronomia.ttcfinchstation.model;

import com.avastronomia.ttcfinchstation.model.weather.Main;
import com.avastronomia.ttcfinchstation.model.weather.WeatherResponse;
import com.avastronomia.ttcfinchstation.service.WeatherDataService;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.avastronomia.ttcfinchstation.util.Constants.WEATHER_APIKEY;
import static com.avastronomia.ttcfinchstation.util.Constants.WEATHER_LOCATION;
import static com.avastronomia.ttcfinchstation.util.Constants.WEATHER_UNIT;

@Singleton
public class WeatherRepository {
    private final WeatherDataService weatherDataService;

    @Inject
    public WeatherRepository(WeatherDataService weatherDataService){
        this.weatherDataService = weatherDataService;
    }

    public Single<Double> getTemperature(){
        return weatherDataService.getCurrentWeather(WEATHER_LOCATION, WEATHER_APIKEY, WEATHER_UNIT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(WeatherResponse::getMain)
                .map(Main::getTemp);
    }
}
