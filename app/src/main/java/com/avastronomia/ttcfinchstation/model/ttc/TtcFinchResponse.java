
package com.avastronomia.ttcfinchstation.model.ttc;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TtcFinchResponse {

    @SerializedName("time")
    @Expose
    private Long time;
    @SerializedName("stops")
    @Expose
    private List<Stop> stops = null;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("name")
    @Expose
    private String name;

    public TtcFinchResponse(Long time, List<Stop> stops, String uri, String name) {
        super();
        this.time = time;
        this.stops = stops;
        this.uri = uri;
        this.name = name;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
