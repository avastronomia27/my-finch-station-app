
package com.avastronomia.ttcfinchstation.model.ttc;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Route {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("route_group_id")
    @Expose
    private String routeGroupId;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("stop_times")
    @Expose
    private List<StopTime> stopTimes = null;

    public Route(String name, String routeGroupId, String uri, List<StopTime> stopTimes) {
        this.name = name;
        this.routeGroupId = routeGroupId;
        this.uri = uri;
        this.stopTimes = stopTimes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRouteGroupId() {
        return routeGroupId;
    }

    public void setRouteGroupId(String routeGroupId) {
        this.routeGroupId = routeGroupId;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public List<StopTime> getStopTimes() {
        return stopTimes;
    }

    public void setStopTimes(List<StopTime> stopTimes) {
        this.stopTimes = stopTimes;
    }

}
