
package com.avastronomia.ttcfinchstation.model.alert;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Articles implements Parcelable
{

    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("totalResults")
    @Expose
    private Integer totalResults;
    @SerializedName("pages")
    @Expose
    private Integer pages;
    @SerializedName("results")
    @Expose
    private List<Result> results = null;
    public final static Parcelable.Creator<Articles> CREATOR = new Creator<Articles>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Articles createFromParcel(Parcel in) {
            return new Articles(in);
        }

        public Articles[] newArray(int size) {
            return (new Articles[size]);
        }

    }
    ;

    protected Articles(Parcel in) {
        this.page = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.totalResults = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.pages = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.results, (Result.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     * 
     */
    public Articles() {
    }

    /**
     * 
     * @param totalResults
     * @param pages
     * @param page
     * @param results
     */
    public Articles(Integer page, Integer totalResults, Integer pages, List<Result> results) {
        super();
        this.page = page;
        this.totalResults = totalResults;
        this.pages = pages;
        this.results = results;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(page);
        dest.writeValue(totalResults);
        dest.writeValue(pages);
        dest.writeList(results);
    }

    public int describeContents() {
        return  0;
    }

}
