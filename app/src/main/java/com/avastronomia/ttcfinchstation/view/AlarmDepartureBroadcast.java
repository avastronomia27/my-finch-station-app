package com.avastronomia.ttcfinchstation.view;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.avastronomia.ttcfinchstation.R;

import static com.avastronomia.ttcfinchstation.util.Constants.SHAPE_NAME;

public class AlarmDepartureBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String shapeName = intent.getStringExtra(SHAPE_NAME);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "finchNotify")
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(shapeName.toString())
                .setContentText(context.getString(R.string.message_departure_notification_body))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(100, builder.build());
    }


}
