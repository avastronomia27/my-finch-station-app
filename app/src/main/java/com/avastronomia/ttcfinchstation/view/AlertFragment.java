package com.avastronomia.ttcfinchstation.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.adapter.ArticleAdapter;
import com.avastronomia.ttcfinchstation.databinding.FragmentAlertBinding;
import com.avastronomia.ttcfinchstation.di.FragmentComponent;
import com.avastronomia.ttcfinchstation.model.alert.Result;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;
import com.avastronomia.ttcfinchstation.viewModel.AlertViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AlertFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @Inject
    ActivityRouter activityRouter;

    @Inject
    AlertViewModel alertViewModel;

    private FragmentAlertBinding fragmentAlertBinding;

    private ArticleAdapter articleAdapter;
    private RecyclerView recyclerView;
    private List<Result> resultList;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentComponent.component(this).inject(this);
        fragmentAlertBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_alert, container, false);
        View root = fragmentAlertBinding.getRoot();
        getActivity().setTitle(getResources().getString(R.string.title_notification_alert));
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        fragmentAlertBinding.setViewModel(alertViewModel);
        initSwipeLayout();
        initRecyclerView();
        getArticleList();
    }

    @Override
    public void onRefresh() {
        alertViewModel.loadAllAlerts();
    }

    private void initSwipeLayout(){
        swipeRefreshLayout = fragmentAlertBinding.slAlert;
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getArticleList(){
        alertViewModel.getResultsLiveData().observe(getViewLifecycleOwner(), new Observer<List<Result>>() {
            @Override
            public void onChanged(List<Result> resultsFromLiveData) {
                resultList = resultsFromLiveData;
                displayOnRecyclerView();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void initRecyclerView(){
        resultList = new ArrayList<>();
        articleAdapter = new ArticleAdapter(activityRouter, resultList, this);
        recyclerView = fragmentAlertBinding.rvAlertList;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(articleAdapter);
    }

    private void displayOnRecyclerView(){articleAdapter.setResultList(resultList);}
}
