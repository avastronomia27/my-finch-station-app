package com.avastronomia.ttcfinchstation.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.adapter.ShapeAdapter;
import com.avastronomia.ttcfinchstation.adapter.StopTimeAdapter;
import com.avastronomia.ttcfinchstation.databinding.ActivityShapeBinding;
import com.avastronomia.ttcfinchstation.di.ActivityComponent;
import com.avastronomia.ttcfinchstation.model.ttc.Shape;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;
import com.avastronomia.ttcfinchstation.viewModel.ShapeViewModel;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_ROUTES_NAME;
import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_ROUTES_URI;
import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_STOPS_URI;

public class ShapeActivity extends BaseActivity implements StopTimeAdapter.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    @Inject
    ActivityRouter router;

    @Inject
    ShapeViewModel shapeViewModel;
    private ActivityShapeBinding activityShapeBinding;

    private ShapeAdapter shapeAdapter;
    private RecyclerView shapeRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    private List<Shape> shapeList;

    private String stopUri = "";
    private String routeUri = "";
    private String routeName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        ActivityComponent.component(this).inject(this);

        activityShapeBinding = DataBindingUtil.setContentView(this, R.layout.activity_shape);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(getIntent().hasExtra(EXTRA_INTENT_ROUTES_URI)){
            stopUri = getIntent().getStringExtra(EXTRA_INTENT_STOPS_URI);
            routeUri = getIntent().getStringExtra(EXTRA_INTENT_ROUTES_URI);
            routeName = getIntent().getStringExtra(EXTRA_INTENT_ROUTES_NAME);
        }
        getSupportActionBar().setTitle(routeName);
        initSwipeLayout();
        initRecyclerView();
        getShapeList();

        shapeViewModel.loadAllShape(stopUri, routeUri);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                shapeAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onRefresh() {
        shapeViewModel.loadAllShape(stopUri, routeUri);
    }

    private void initSwipeLayout(){
        swipeRefreshLayout = activityShapeBinding.slShape;
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getShapeList(){
        shapeViewModel.getShapesLiveData().observe(this, shapes -> {
            shapeList = shapes;
            displayOnRecyclerView();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    private void initRecyclerView(){
        shapeList = new ArrayList<>();
        shapeAdapter = new ShapeAdapter(this,
                shapeList,
                this);
        shapeRecyclerView = activityShapeBinding.rvShape;
        shapeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        shapeRecyclerView.setItemAnimator(new DefaultItemAnimator());
        shapeRecyclerView.setAdapter(shapeAdapter);
    }

    private void displayAlarmDialog(String departureTime,
                                    String shapeName,
                                    long departureTimestamp){

        String tempoStr;
        long tempoTimeRemaining = compareTimestamp(departureTimestamp);

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(shapeName);
        if(tempoTimeRemaining < 0){
            tempoStr = getResources().getString(R.string.message_departure_time_left);
        }
        else if(tempoTimeRemaining < 300000 && tempoTimeRemaining >= 1){
            tempoStr = getResources().getString(R.string.message_departure_time_less, shapeName);
        }else{
            tempoStr = getResources().getString(R.string.message_departure_time_more, departureTime);
            builder.setPositiveButton(getResources().getString(R.string.label_notify), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Snackbar.make(activityShapeBinding.getRoot(), "You will be notified", Snackbar.LENGTH_LONG).show();
                    router.setAlarmDepartureDialog(shapeName, departureTimestamp);
                    dialog.dismiss();
                }
            });
        }
        builder.setMessage(tempoStr);
        builder.setNegativeButton(getResources().getString(R.string.label_dismiss), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(tempoTimeRemaining < 0){
                    getShapeList();
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void displayOnRecyclerView(){
        shapeAdapter.setShapeList(shapeList);
    }


    @Override
    public void onItemClicked(View vh,
                              String departureTime,
                              String shapeName,
                              long departureTimestamp) {
        displayAlarmDialog(departureTime, shapeName, departureTimestamp);
    }

    private long compareTimestamp(long departureTimestamp){
        return (departureTimestamp*1000) - System.currentTimeMillis();
    }


    public void onDestroy(){
        super.onDestroy();
    }
}
