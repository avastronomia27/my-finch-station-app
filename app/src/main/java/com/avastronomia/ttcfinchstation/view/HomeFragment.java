package com.avastronomia.ttcfinchstation.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.ViewModelProvider;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.databinding.FragmentHomeBinding;
import com.avastronomia.ttcfinchstation.di.FragmentComponent;
import com.avastronomia.ttcfinchstation.viewModel.HomeViewModel;

import javax.inject.Inject;

public class HomeFragment extends Fragment {

    @Inject
    HomeViewModel homeViewModel;

    private FragmentHomeBinding fragmentHomeBinding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentComponent.component(this).inject(this);
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        getActivity().setTitle(R.string.title_finch_station);

        View root = fragmentHomeBinding.getRoot();
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        fragmentHomeBinding.setViewModel(homeViewModel);
    }
}
