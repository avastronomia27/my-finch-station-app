package com.avastronomia.ttcfinchstation.view;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.adapter.StopAdapter;
import com.avastronomia.ttcfinchstation.databinding.FragmentStationBinding;
import com.avastronomia.ttcfinchstation.di.FragmentComponent;
import com.avastronomia.ttcfinchstation.model.ttc.Stop;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;
import com.avastronomia.ttcfinchstation.viewModel.StationViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class StationFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @Inject
    ActivityRouter router;
    @Inject
    StationViewModel stationViewModel;

    private FragmentStationBinding fragmentStationBinding;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private StopAdapter stopAdapter;
    private List<Stop> stops;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentComponent.component(this).inject(this);
        fragmentStationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_station, container, false);
        View root = fragmentStationBinding.getRoot();
        getActivity().setTitle(R.string.title_finch_station);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        fragmentStationBinding.setViewModel(stationViewModel);
        initRecyclerView();
        initSwipeLayout();
        setHasOptionsMenu(true);
        getStopList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                stopAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    @Override
    public void onRefresh() {
        stationViewModel.loadAllStops();
    }

    private void initSwipeLayout(){
        swipeRefreshLayout = fragmentStationBinding.slStation;
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void getStopList(){
        stationViewModel.getStopLiveData().observe(getViewLifecycleOwner(), new Observer<List<Stop>>() {
            @Override
            public void onChanged(@Nullable  List<Stop> stopsFromLiveData) {
                stops = stopsFromLiveData;
                displayOnRecyclerView();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void initRecyclerView(){
        stops = new ArrayList<>();
        stopAdapter = new StopAdapter(router, stops);
        recyclerView = fragmentStationBinding.rvStop;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(stopAdapter);
    }

    private void displayOnRecyclerView(){
        stopAdapter.setStopsList(stops);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        stationViewModel.clear();
    }



}
