package com.avastronomia.ttcfinchstation.view;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.adapter.RouteAdapter;
import com.avastronomia.ttcfinchstation.databinding.ActivityRouteBinding;
import com.avastronomia.ttcfinchstation.di.ActivityComponent;
import com.avastronomia.ttcfinchstation.model.ttc.Route;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;
import com.avastronomia.ttcfinchstation.viewModel.RouteViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_STOPS_NAME;
import static com.avastronomia.ttcfinchstation.util.Constants.EXTRA_INTENT_STOPS_URI;

public class RouteActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{
    private ActivityRouteBinding activityRouteBinding;

    @Inject
    ActivityRouter router;
    @Inject
    RouteViewModel routeViewModel;

    private RecyclerView recyclerView;
    private RouteAdapter routeAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Route> routeList;
    private String stopUri = "";
    private String stopName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);
        ActivityComponent.component(this).inject(this);
        activityRouteBinding = DataBindingUtil.setContentView(this, R.layout.activity_route);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(getIntent().hasExtra(EXTRA_INTENT_STOPS_URI)){
            stopUri = getIntent().getStringExtra(EXTRA_INTENT_STOPS_URI);
            stopName = getIntent().getStringExtra(EXTRA_INTENT_STOPS_NAME);
        }

        activityRouteBinding.setViewModel(routeViewModel);
        routeViewModel.loadAllRoute(stopUri);
        getSupportActionBar().setTitle(stopName);

        initSwipeLayout();
        initRecyclerView();
        getRouteList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                routeAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void initSwipeLayout(){
        swipeRefreshLayout = activityRouteBinding.slRoute;
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        routeViewModel.loadAllRoute(stopUri);
    }

    private void getRouteList(){
        routeViewModel.getRouteLiveData().observe(this, routes -> {
                routeList = routes;
                displayOnRecyclerView();
                swipeRefreshLayout.setRefreshing(false);
        });
    }

    private void initRecyclerView(){
        routeList = new ArrayList<>();
        routeAdapter = new RouteAdapter(router, routeList, stopUri);
        recyclerView = activityRouteBinding.rvRoute;
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(routeAdapter);
    }

    private void displayOnRecyclerView(){
        routeAdapter.setRouteList(routeList);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        //routeViewModel.clear();
    }
}
