package com.avastronomia.ttcfinchstation.view;

import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;

import com.avastronomia.ttcfinchstation.R;
import com.avastronomia.ttcfinchstation.di.ActivityComponent;
import com.avastronomia.ttcfinchstation.util.ActivityRouter;

import javax.inject.Inject;

public class SplashScreenActivity extends AppCompatActivity {

    @Inject
    ActivityRouter router;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splashscreen);
        ActivityComponent.component(this).inject(this);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                    router.startMainActivity();

            }
        }, 3000);

    }
}
