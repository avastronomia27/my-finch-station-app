package com.avastronomia.ttcfinchstation;

import android.app.Application;
import android.content.Context;

import com.avastronomia.ttcfinchstation.di.AppComponent;

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent= AppComponent.component(this);
        appComponent.inject(this);

    }

    public static AppComponent component(Context context){
        return ((App)context.getApplicationContext()).appComponent;
    }
}
